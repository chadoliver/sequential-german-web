// import * as _ from 'lodash';

import {Theatre} from './Theatre';
import {TrackCache} from './TrackCache';

export enum TrackState {
	EMPTY,
	LOADING,
	PLAYING,
	PAUSED
}

export interface AudioManagerState {
	activeTrackId: string | null;
	trackState: TrackState;
	trackProgress: number;
}

interface TrackClock {
	time: number;
	position: number;
}

export type Listener =  (state: AudioManagerState) => void;

export class AudioManager {
	private static source: AudioBufferSourceNode | null = null;
	private static trackClock: TrackClock | null = null;
	private static progressInterval: any;

	private static listeners: Set<Listener> = new Set();
	private static state: AudioManagerState = {
		activeTrackId: null,
		trackState: TrackState.EMPTY,
		trackProgress: 0
	};

	public static register(listener: Listener) {
		this.listeners.add(listener);
	}

	private static setState(newState: Partial<AudioManagerState>): void {
		let changed = false;
		if (newState.activeTrackId !== undefined && this.state.activeTrackId !== newState.activeTrackId) {
			changed = true;
			this.state.activeTrackId = newState.activeTrackId;
		}
		if (newState.trackState !== undefined && this.state.trackState !== newState.trackState) {
			changed = true;
			this.state.trackState = newState.trackState;
		}
		if (newState.trackProgress !== undefined && this.state.trackProgress !== newState.trackProgress) {
			changed = true;
			this.state.trackProgress = newState.trackProgress;
		}

		if (changed) {
			this.listeners.forEach((listener) => {
				listener(this.state);
			});
		}
	}

	public static async toggle(id: string) {
		if (this.state.activeTrackId === id) {
			if (this.state.trackState === TrackState.PLAYING || this.state.trackState === TrackState.LOADING) {
				this.pauseActiveTrack();
			} else {
				await this.playActiveTrack();
			}
		} else {
			await this.playDifferentTrack(id);
		}
	}

	private static async playActiveTrack() {
		if (this.state.activeTrackId === null) {
			throw new Error('Cannot call .playActiveTrack when state.activeTrackId is null');
		}

		await this.createSourceNode(this.state.activeTrackId);
		this.playTrack();
	}

	private static async playDifferentTrack(id: string) {
		clearInterval(this.progressInterval);
		this.destroySourceNode();

		this.setState({
			activeTrackId: id,
			trackState: TrackState.LOADING
		});
		await this.createSourceNode(id);
		this.trackClock = {
			position: 0,
			time: Theatre.get().currentTime
		};

		this.playTrack();
	}

	private static playTrack() {
		if (this.source === null) {
			throw new Error('Cannot call .playTrack when source is null');
		}
		if (this.trackClock === null) {
			throw new Error('Cannot call .playTrack when trackClock is null');
		}

		this.source.start(0, this.trackClock.position);
		this.trackClock = {
			position: this.trackClock.position,
			time: Theatre.get().currentTime
		};
		this.setState({
			trackState: TrackState.PLAYING,
			trackProgress: this.trackClock.position
		});

		this.progressInterval = setInterval(this.onTick, 10);
	}

	private static pauseActiveTrack() {
		if (this.source === null) {
			throw new Error('Cannot call .pauseActiveTrack when source is null');
		}
		if (this.trackClock === null) {
			throw new Error('Cannot call .pauseActiveTrack when trackClock is null');
		}

		clearInterval(this.progressInterval);

		const newTime = Theatre.get().currentTime;
		const newPosition = this.trackClock.position + (Theatre.get().currentTime - this.trackClock.time);
		this.trackClock = {
			time: newTime,
			position: newPosition
		};

		this.destroySourceNode();
		this.setState({
			trackState: TrackState.PAUSED
		});
	}

	private static async createSourceNode(id: string): Promise<void> {
		this.source = await TrackCache.getAudioSource(id);
		this.source.addEventListener('ended', this.onSourceEnded);
	}

	private static async destroySourceNode(): Promise<void> {
		if (this.source !== null) {
			this.source.removeEventListener('ended', this.onSourceEnded);
			this.source.stop();
			this.source = null;
		}
	}

	public static onSourceEnded() {
		if (AudioManager.source !== null) {
			AudioManager.source.stop();
			AudioManager.source = null;
		}
		clearInterval(AudioManager.progressInterval);

		AudioManager.trackClock = {
			position: 0,
			time: Theatre.get().currentTime
		};
		AudioManager.setState({
			trackState: TrackState.PAUSED,
			trackProgress: 0
		});
	}

	public static onTick() {
		if (AudioManager.source && AudioManager.trackClock) {
			const context = Theatre.get();
			const latency = (context.baseLatency || 0) + (context.outputLatency || 0);
			AudioManager.setState({
				trackProgress: AudioManager.trackClock.position + (context.currentTime - AudioManager.trackClock.time) - latency
			});
		}
	}
}
