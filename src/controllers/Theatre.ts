export class Theatre {
	private static context: AudioContext = new AudioContext();

	public static get(): AudioContext {
		return this.context;
	}
}
