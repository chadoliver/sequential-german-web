import {HOST} from '../config';
import {Theatre} from './Theatre';

export class TrackCache {
	private static cache: Map<string, Promise<AudioBuffer>> = new Map();

	private static async loadData(id: string): Promise<AudioBuffer> {
		const response = await fetch(`${HOST}/audio/${id}`);
		const data = await response.arrayBuffer();
		return Theatre.get().decodeAudioData(data);
	}

	public static getAudioBuffer(id: string): Promise<AudioBuffer> {
		if (this.cache.has(id)) {
			return this.cache.get(id) as Promise<AudioBuffer>;
		} else {
			const data = this.loadData(id);	// this is intentionally not awaited
			this.cache.set(id, data);
			return data;
		}
	}

	public static async getAudioSource(id: string): Promise<AudioBufferSourceNode> {
		const source = Theatre.get().createBufferSource();
		source.buffer = await this.getAudioBuffer(id);
		source.connect(Theatre.get().destination);
		return source;
	}
}

