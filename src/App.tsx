import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components';

import {Navbar} from './view/Navbar';
import {FragmentPage} from './view/pages/fragment/FragmentPage';
import {LessonsIndexPage} from './view/pages/index/LessonsIndexPage';
import {LessonPage} from './view/pages/lesson/LessonPage';

const GlobalStyles = styled.div`
	font-family: Roboto, sans-serif;
`;

const App = () => (
	<Router>
		<GlobalStyles>
			<Navbar />
			<Route exact path="/" component={LessonsIndexPage} />
			<Route path="/lesson/:id" component={LessonPage} />
			<Route path="/fragment/:id" component={FragmentPage} />
		</GlobalStyles>
	</Router>
);

export default App;
