import * as React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import * as colorscheme from '../../colorscheme';

const OuterContainer = styled(Link)`
	padding: 0;
	align-self: stretch;
	display: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
	text-decoration: none;
`;

interface InnerContainerProps {
	isFocused: boolean;
}

const InnerContainer = styled.div`
	font-size: 16px;
	padding: 0 15px;
	color: ${(props: InnerContainerProps) => {
		return props.isFocused ? colorscheme.mediaControls.toString() : colorscheme.white.toString();
	}};
`;

export interface EditButtonProps {
	isFocused?: boolean;
	fragmentId: string;
}

export const EditButton: React.SFC<EditButtonProps> = ({isFocused, fragmentId}) => {
	return <OuterContainer to={`/fragment/${fragmentId}`}>
		<InnerContainer isFocused={isFocused || false}>
			Edit
		</InnerContainer>
	</OuterContainer>;
};
