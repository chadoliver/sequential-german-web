import * as React from 'react';
import {match as routerMatch} from 'react-router-dom';

import {HOST} from '../../../config';
import CenteredColumn from '../../CenteredColumn';
import {FocusableRow} from '../../common/FocusableRow';
import {PlayButton} from '../../common/PlayButton';
import {Sentence} from '../../common/Sentence';
import {VerticalList} from '../../common/VerticalList';
import {EditButton} from './EditButton';
import {Heading} from './Heading';

export interface ComposedFragment {
	id: string;
	type: string;
	audioId: string;
	transcription: string;
}

interface LessonPageState {
	isPending: boolean;
	fragments: ComposedFragment[];
}

export interface LessonPageProps {
	match: routerMatch<{
		id: string;
	}>;
}

export class LessonPage extends React.Component<LessonPageProps, LessonPageState> {
	public state: LessonPageState;

	constructor(props: LessonPageProps) {
		super(props);
		this.state = {
			isPending: true,
			fragments: []
		};
	}

	public async componentDidMount() {
		const lessonId = this.props.match.params.id;
		const fragments = await (await fetch(`${HOST}/api/page/lesson/${lessonId}`)).json();
		this.setState({
			isPending: false,
			fragments
		})
	}

	public render() {
		const sentences = this.state.fragments.filter((fragment) => {
			return fragment.type === 'Sentence';
		});

		return (
			<CenteredColumn width={'600px'}>
				<VerticalList>
					<Heading fragments={this.state.fragments} />
					{sentences.map(({id, audioId, transcription}) => (
						<FocusableRow key={id} audioId={audioId}>
							<PlayButton audioId={audioId} />
							<Sentence text={transcription} />
							<EditButton fragmentId={id} />
						</FocusableRow>
					))}
				</VerticalList>
			</CenteredColumn>
		);
	}
}
