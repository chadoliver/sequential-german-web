import * as _ from 'lodash';
import * as React from 'react';
import styled from 'styled-components';

import * as colorscheme from '../../colorscheme';
import {ComposedFragment} from './LessonPage';

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	padding: 16px 16px 24px 41px;
	margin: 0;
	background-color: ${colorscheme.white.toString()};
	width: 100%;
	color: black;
`;

const Title = styled.div`
	font-size: 24px;
`;

const Subtitle = styled.div`
	margin-top: 8px;
	color: ${colorscheme.subtitle.toString()};
`;

export interface FragmentListHeadingProps {
	fragments: ComposedFragment[]
}

export const Heading: React.SFC<FragmentListHeadingProps> = ({fragments}) => {
	const title = _.find(fragments, {type: 'Title'});
	const subtitle = _.find(fragments, {type: 'Ordinal'});
	if (title === undefined || subtitle === undefined) {
		return null;
	} else {
		return (
			<Container>
				<Title>{title.transcription}</Title>
				<Subtitle>{subtitle.transcription}</Subtitle>
			</Container>
		);
	}
};
