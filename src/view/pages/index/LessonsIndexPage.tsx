import * as React from 'react';

import {HOST} from '../../../config';
import CenteredColumn from '../../CenteredColumn';
import {VerticalList} from '../../common/VerticalList';
import {LessonListElement} from './LessonListElement';

interface ComposedLesson {
	id: string;
	ordinal: number;
	title: string;
}

interface LessonListState {
	isPending: boolean;
	lessons: ComposedLesson[];
}

export class LessonsIndexPage extends React.Component<{}, LessonListState> {
	public state: LessonListState;

	constructor(props: {}) {
		super(props);
		this.state = {
			isPending: true,
			lessons: []
		};
	}

	public async componentDidMount() {
		const lessons = await (await fetch(`${HOST}/api/page/lessons`)).json();
		this.setState({
			isPending: false,
			lessons
		})
	}

	public render() {
		return (
			<CenteredColumn width={'600px'}>
				<VerticalList>
					{this.state.lessons.map(({id, ordinal, title}) =>
						<LessonListElement key={id} id={id} ordinal={ordinal} title={title} />
					)}
				</VerticalList>
			</CenteredColumn>
		);
	}
}
