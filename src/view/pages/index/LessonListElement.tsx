import * as React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import * as colorscheme from '../../colorscheme';

export interface StyledListElementProps {
	width: string;
}

const ListElement = styled(Link)`
	display: block;
	padding: 16px;
	margin: 0;
	background-color: ${colorscheme.white.toString()};
	width: 100%;
	color: black;
	text-decoration: none;
	&:hover {
		background-color: ${colorscheme.listItemBackground.toString()};
		cursor: pointer;
	}
`;

export interface LessonListElementProps {
	id: string;
	ordinal: number;
	title: string;
}

export const LessonListElement: React.SFC<LessonListElementProps> = ({id, title}) => {
	return <ListElement to={`/lesson/${id}`}>{title}</ListElement>;
};
