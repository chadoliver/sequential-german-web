import * as _ from 'lodash';
import * as React from 'react';

export interface CenterlineProps {
	length: number;
	stroke: string;
}

export class Centerline extends React.Component<CenterlineProps> {

	constructor(props: CenterlineProps) {
		super(props);
	}

	public shouldComponentUpdate(nextProps: CenterlineProps) {
		return _.some([
			this.props.length !== nextProps.length,
			this.props.stroke !== nextProps.stroke
		]);
	}

	public render() {
		return <line
			x1="0"
			y1="1"
			x2={this.props.length}
			y2="1"
			stroke={this.props.stroke}
			strokeWidth="1"
			vectorEffect="non-scaling-stroke"
		/>
	}
}
