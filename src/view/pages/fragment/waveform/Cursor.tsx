import * as _ from 'lodash';
import * as React from 'react';

export interface CursorProps {
	height: number;
	mouseXPosition?: number;
}

export const Cursor: React.SFC<CursorProps> = ({mouseXPosition, height}) => {
	if (mouseXPosition === undefined) {
		return null;
	} else {
		return <line x1={mouseXPosition} y1={0} x2={mouseXPosition} y2={height} stroke={'#000'}/>;
	}
};
