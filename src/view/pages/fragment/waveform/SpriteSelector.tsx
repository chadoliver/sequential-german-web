import * as _ from 'lodash';
import * as React from 'react';
import ReactCursorPosition from 'react-cursor-position';
import styled from 'styled-components';

import {TrackCache} from '../../../../controllers/TrackCache';
import {InternalSpriteSelector} from './InternalSpriteSelector';

const StyledReactCursorPosition = styled(ReactCursorPosition)`
	margin: 8px;
`;

interface LoadingWrapperProps {
	width: number;
	height: number;
}
const LoadingWrapper = styled.div`
	width: ${(props: LoadingWrapperProps) => props.width}px;
	height: ${(props: LoadingWrapperProps) => props.height}px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

interface InputPositionProps {
	isPositionOutside: boolean;
	position: {
		x: number;
		y: number;
	}
}

interface OutputPositionProps {
	mouseXPosition: number | undefined;
}

function mapChildProps(props: InputPositionProps): OutputPositionProps {
	return {
		mouseXPosition: props.isPositionOutside ? undefined : props.position.x
	};
}

export interface SpriteSelectorProps {
	audioId: string;
	width: number;
	height: number;
}

interface SpriteSelectorState {
	channelData: Float32Array | null;
	duration: number;
}

export class SpriteSelector extends React.Component<SpriteSelectorProps, SpriteSelectorState> {
	public state: SpriteSelectorState;

	constructor(props: SpriteSelectorProps) {
		super(props);
		this.state = {
			channelData: null,
			duration: 0
		};
	}

	public async componentDidMount() {
		const audioData = await TrackCache.getAudioBuffer(this.props.audioId);
		const channelData = audioData.getChannelData(0);
		this.setState({
			channelData,
			duration: audioData.duration
		});
	}

	public shouldComponentUpdate(nextProps: SpriteSelectorProps, nextState: SpriteSelectorState) {
		return _.some([
			this.props.audioId !== nextProps.audioId,
			this.props.width !== nextProps.width,
			this.props.height !== nextProps.height,
			this.state.channelData !== nextState.channelData,
			this.state.duration !== nextState.duration,
		]);
	}

	public render() {
		if (this.state.channelData) {
			return <StyledReactCursorPosition mapChildProps={mapChildProps}>
				<InternalSpriteSelector
					channelData={this.state.channelData as any}
					duration={this.state.duration}
					width={this.props.width}
					height={this.props.height}
				/>
			</StyledReactCursorPosition>;
		} else {
			return <LoadingWrapper width={this.props.width} height={this.props.height}>
				<div>
					loading...
				</div>
			</LoadingWrapper>
		}
	}
}


