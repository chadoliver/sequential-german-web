import * as _ from 'lodash';
import * as React from 'react';

import * as colorscheme from '../../../colorscheme';
import {Centerline} from './Centerline';
import {ProgressMasks} from './ProgressMasks';
import {Slice} from './Slice';
import {WaveformMask} from './WaveformMask';

export interface InternalSpriteSelectorProps {
	channelData: Float32Array;
	duration: number;
	width: number;
	height: number;
	mouseXPosition?: number;
}

interface InternalSpriteSelectorState {
	window: {
		start: number;
		end: number;
	};
}

export class InternalSpriteSelector extends React.Component<InternalSpriteSelectorProps, InternalSpriteSelectorState> {
	public state: InternalSpriteSelectorState;

	constructor(props: InternalSpriteSelectorProps) {
		super(props);
		this.state = {
			window: {
				start: 0,
				end: props.channelData.length
			}
		};
	}

	public shouldComponentUpdate(nextProps: InternalSpriteSelectorProps, nextState: InternalSpriteSelectorState) {
		return _.some([
			this.props.channelData !== nextProps.channelData,
			this.props.duration !== nextProps.duration,
			this.props.width !== nextProps.width,
			this.props.height !== nextProps.height,
			this.props.mouseXPosition !== nextProps.mouseXPosition,
			this.state.window.start !== nextState.window.start,
			this.state.window.end !== nextState.window.end
		]);
	}

	public onWheel = (event: React.WheelEvent<SVGSVGElement>) => {
		const scaleFactor = 0.7 ** (0 - Math.sign(event.deltaY));

		this.setState((state, props) => {
			const windowWidth = state.window.end - state.window.start;
			const centerPoint = state.window.start + windowWidth * ((props.mouseXPosition || (props.width / 2)) / props.width);
			return {
				window: {
					start: Math.max(0, centerPoint - (centerPoint - state.window.start) * scaleFactor),
					end: Math.min(props.channelData.length, centerPoint - (centerPoint - state.window.end) * scaleFactor)
				},
			};
		});
	};

	public onMouseDown = (event: React.MouseEvent<SVGSVGElement>) => {
		console.log('mouse down:', this.props.mouseXPosition);
	};

	public onMouseUp = (event: React.MouseEvent<SVGSVGElement>) => {
		console.log('mouse up:', this.props.mouseXPosition);
	};

	public render() {
		return (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width={this.props.width}
				height={this.props.height}
				viewBox={`${this.state.window.start} 0 ${this.state.window.end - this.state.window.start} 2`}
				preserveAspectRatio="none"
				onWheel={this.onWheel}
				onMouseDown={this.onMouseDown}
				onMouseUp={this.onMouseUp}
			>
				<defs>
					<WaveformMask channelData={this.props.channelData} window={this.state.window}/>
					<ProgressMasks
						trackLength={this.props.channelData.length}
						trackDuration={this.props.duration}
					/>
					<g id="slices" fill={String(colorscheme.tangerine)}>
						<Slice start={0.9} end={2} trackLength={this.props.channelData.length} trackDuration={this.props.duration}/>
						<Slice start={4} end={5} trackLength={this.props.channelData.length} trackDuration={this.props.duration}/>
					</g>
				</defs>
				<Centerline length={this.props.channelData.length} stroke="#bbb"/>
				<g clipPath="url(#future)">
					<g mask="url(#waveform-path)">
						<rect x="0" y="0" width={this.props.channelData.length} height="2" fill="#bbb"/>
					</g>
				</g>
				<use href="#slices" opacity={0.07}/>
				<g clipPath="url(#past)">
					<g mask="url(#waveform-path)">
						<rect x="0" y="0" width={this.props.channelData.length} height="2" fill="black"/>
						<use href="#slices" opacity={1}/>
					</g>
					<rect x="0" y="0" width={this.props.channelData.length} height="2" fill="black" fillOpacity={0.1}/>
				</g>
				<use href="#progress-cursor" />
			</svg>
		);
	}
}
