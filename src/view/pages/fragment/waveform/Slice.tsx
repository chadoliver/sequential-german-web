import * as _ from 'lodash';
import * as React from 'react';

export interface SliceProps {
	start: number;				// in seconds
	end: number;				// in seconds
	trackLength: number; 		// in number of samples
	trackDuration: number; 	// in seconds
}

export class Slice extends React.Component<SliceProps> {

	constructor(props: SliceProps) {
		super(props);
	}

	public shouldComponentUpdate(nextProps: SliceProps) {
		return _.some([
			this.props.start !== nextProps.start,
			this.props.end !== nextProps.end,
			this.props.trackLength !== nextProps.trackLength,
			this.props.trackDuration !== nextProps.trackDuration
		]);
	}

	public render() {
		const scaleFactor = this.props.trackLength / this.props.trackDuration;
		return <rect x={scaleFactor * this.props.start} y="0" width={scaleFactor * (this.props.end - this.props.start)} height="2"/>;
	}
}
