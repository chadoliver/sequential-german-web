import * as _ from 'lodash';
import * as React from 'react';
import * as simplify from 'simplify-js'

interface Point {
	x: number;
	y: number;
}

export interface WaveformMaskProps {
	channelData: Float32Array;
	window: {
		start: number;
		end: number;
	};
}

export class WaveformMask extends React.Component<WaveformMaskProps> {
	private VERTICAL_SCALE = 5_000;
	private TOLERANCE = 50;

	constructor(props: WaveformMaskProps) {
		super(props);
	}

	public shouldComponentUpdate(nextProps: WaveformMaskProps) {
		return _.some([
			this.props.channelData !== nextProps.channelData,
			_.floor(this.props.window.start) !== _.floor(nextProps.window.start),
			_.floor(this.props.window.end) !== _.floor(nextProps.window.end),
		]);
	}

	public render() {
		const scaledTolerance = this.TOLERANCE * (this.props.window.end - this.props.window.start) / this.props.channelData.length;

		const rawPoints = this.getPointsInWindow();
		const simplifiedPoints: Point[] = simplify(rawPoints, scaledTolerance);
		const pointsAsString = simplifiedPoints.map(({x, y}) => `${x},${y / this.VERTICAL_SCALE}`).join(' ');

		return 	<mask id="waveform-path" >
			<polyline points={pointsAsString} strokeWidth="1" stroke="white" fill="none" vectorEffect="non-scaling-stroke"/>
		</mask>;
	}

	private getPointsInWindow(): Point[] {
		const startIndex = Math.max(0, _.floor(this.props.window.start) - 1);
		const endIndex = Math.min(this.props.channelData.length, _.floor(this.props.window.end) + 1);

		const points = [];
		for (let x = startIndex; x < endIndex; x++) {
			const y = this.VERTICAL_SCALE * (this.props.channelData[x] + 1);
			points.push({x, y});
		}
		return points;
	}
}
