import * as _ from 'lodash';
import * as React from 'react';
import {AudioManager, AudioManagerState} from '../../../../controllers/AudioManager';

export interface ProgressMasksProps {
	trackLength: number; 		// number of samples
	trackDuration: number; 	// time
}

export interface ProgressMasksState {
	progress: number;	// a number, from 0 to 1, indicating how far through the track we are
}

export class ProgressMasks extends React.Component<ProgressMasksProps, ProgressMasksState> {
	public state: ProgressMasksState;

	constructor(props: ProgressMasksProps) {
		super(props);
		this.state = {
			progress: 0
		};
		AudioManager.register(this.onAudioManagerStateChanged);
	}

	public shouldComponentUpdate(nextProps: ProgressMasksProps, nextState: ProgressMasksState) {
		return _.some([
			this.props.trackLength !== nextProps.trackLength,
			this.props.trackDuration !== nextProps.trackDuration,
			this.state.progress !== nextState.progress
		]);
	}

	public onAudioManagerStateChanged = (audioManagerState: AudioManagerState): void => {
		this.setState((_state, props) => {
			return {
				progress: audioManagerState.trackProgress
			}
		});
	};

	public render() {
		const boundary = this.props.trackLength * (this.state.progress / this.props.trackDuration) || 0;
		return <>
			<clipPath id="past">
				<rect x="0" y="0" width={boundary} height="2"/>
			</clipPath>
			<clipPath id="future">
				<rect x={boundary} y="0" width={this.props.trackLength - boundary} height="2"/>
			</clipPath>
			<line id="progress-cursor" x1={boundary} y1="0" x2={boundary} y2="2" stroke="black" vectorEffect="non-scaling-stroke"/>;
		</>;
	}
}
