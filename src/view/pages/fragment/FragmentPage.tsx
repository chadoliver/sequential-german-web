import * as React from 'react';
import {match as routerMatch} from 'react-router-dom';
import styled from 'styled-components';

import {HOST} from '../../../config';
import CenteredColumn from '../../CenteredColumn';
import {FocusableRow} from '../../common/FocusableRow';
import {PlayButton} from '../../common/PlayButton';
import {Sentence} from '../../common/Sentence';
import {VerticalList} from '../../common/VerticalList';
import {SpriteSelector} from './waveform/SpriteSelector';

const StyledFocusableRow = styled(FocusableRow)`
	font-size: 24px;
`;

type PendingState = {
	isPending: true;
}

type LoadedState = {
	isPending: false;
	fragment: {
		type: string;
		audioId: string;
		transcription: string;
	};
}

type FragmentPageState = PendingState | LoadedState;

export interface FragmentPageProps {
	match: routerMatch<{
		id: string;
	}>;
}

export class FragmentPage extends React.Component<FragmentPageProps, FragmentPageState> {
	public state: FragmentPageState;

	constructor(props: FragmentPageProps) {
		super(props);
		this.state = {
			isPending: true
		};
	}

	public async componentDidMount() {
		const fragmentId = this.props.match.params.id;
		const fragment = await (await fetch(`${HOST}/api/page/fragment/${fragmentId}`)).json();
		this.setState({
			isPending: false,
			fragment
		})
	}

	public render() {
		if (this.state.isPending) {
			return null;
		} else {
			const {audioId, transcription} = this.state.fragment;
			return (
				<CenteredColumn width={'600px'}>
					<VerticalList>
						<StyledFocusableRow audioId={audioId}>
							<PlayButton audioId={audioId}/>
							<Sentence text={transcription} />
						</StyledFocusableRow>
						<SpriteSelector audioId={audioId} width={600} height={200}/>
					</VerticalList>
				</CenteredColumn>
			);
		}
	}
}
