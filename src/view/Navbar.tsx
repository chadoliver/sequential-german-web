import * as React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components'

import * as colorscheme from './colorscheme';

export interface NavbarProps {
}

const StyledContainer = styled.div`
	width: 100%;
	background-color: ${colorscheme.tangerine.toString()};
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
`;

const StyledLink = styled(Link)`
	padding: 10px;
	color: white;
	text-decoration: none;
`;

export const Navbar: React.SFC<NavbarProps> = () => {
	return (
		<StyledContainer>
			<StyledLink to="/">Lessons</StyledLink>
			<StyledLink to="/user">User</StyledLink>
			<StyledLink to="/foo">Foo</StyledLink>
		</StyledContainer>
	);
};
