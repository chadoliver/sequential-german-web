import * as React from 'react';
import styled from 'styled-components';

const Container = styled.div`
	flex-grow: 1;
	padding: 16px 0;
`;

export interface SentenceProps {
	isFocused?: boolean;
	text: string;
}

export const Sentence: React.SFC<SentenceProps> = ({isFocused, text}) => {
	return <Container>{text}</Container>;
};
