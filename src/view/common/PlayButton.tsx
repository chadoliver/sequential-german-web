import * as React from 'react';
import styled from 'styled-components';

import {AudioManager, AudioManagerState, TrackState} from '../../controllers/AudioManager';
import * as colorscheme from '../colorscheme';

interface ContainerProps {
	isFocused: boolean;
}

const OuterContainer = styled.div`
	width: 40px;
	padding: 0 0 0 5px;
	align-self: stretch;
	display: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
`;

const InnerContainer = styled.div`
	font-size: 16px;
	color: ${(props: ContainerProps) => {
		return props.isFocused ? colorscheme.mediaControls.toString() : colorscheme.white.toString();
	}};
`;

export interface PlayButtonProps {
	audioId: string;
	isFocused?: boolean;
}

interface PlayButtonState {
	trackState: TrackState
}

export class PlayButton extends React.Component<PlayButtonProps, PlayButtonState> {
	constructor(props: PlayButtonProps) {
		super(props);
		this.state = {
			trackState: TrackState.EMPTY
		};
		AudioManager.register(this.onAudioManagerStateChanged);
	}

	public onAudioManagerStateChanged = (audioManagerState: AudioManagerState): void => {
		this.setState((state, props) => {
			if (audioManagerState.activeTrackId === props.audioId) {
				return{
					trackState: audioManagerState.trackState
				};
			} else {
				return {
					trackState: TrackState.EMPTY
				};
			}
		});
	};

	private onClick = () => {
		AudioManager.toggle(this.props.audioId);
	};

	private get symbol(): string {
		switch (this.state.trackState) {
			case TrackState.LOADING:
				return '—';
			case TrackState.PLAYING:
				return '⏸';
			case TrackState.PAUSED:
				return '▶';
			case TrackState.EMPTY:
				return '▶';
		}
	}

	public render() {
		const isFocused = this.props.isFocused || false;
		return <OuterContainer onClick={this.onClick}>
			<InnerContainer isFocused={isFocused}>
				{this.symbol}
			</InnerContainer>
		</OuterContainer>;
	}
}
