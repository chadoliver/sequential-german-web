import styled from 'styled-components';

export interface ListRowProps {
	flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
	alignItems?: 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch';
}

export const ListRow = styled.div`
	display: flex;
	flex-direction: ${(props: ListRowProps) => {
		return props.flexDirection || 'row';
	}};
	align-items: ${(props: ListRowProps) => {
		return props.alignItems || 'flex-start';
	}};
	padding: 0;
	margin: 0;
	width: 100%;
	color: black;
`;
