import styled from 'styled-components';

export const VerticalList = styled.div`
	padding: 0;
	margin: 32px 0;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	width: 100%;
`;
