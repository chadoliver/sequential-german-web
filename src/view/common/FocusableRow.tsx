import * as React from 'react';
import styled from 'styled-components';

import * as colorscheme from '../colorscheme';
import {ListRow} from './ListRow';

interface StyledListRowProps {
	isFocused: boolean;
}

const StyledListRow = styled(ListRow)`
	background-color: ${(props: StyledListRowProps) => {
		return String(props.isFocused ? colorscheme.listItemBackground : colorscheme.white);
	}};
`;

interface FocusableRowState {
	isFocused: boolean;
}

export interface FocusableRowProps {
	audioId: string;
	className?: string;
}

export class FocusableRow extends React.Component<FocusableRowProps, FocusableRowState> {
	public state: FocusableRowState;

	constructor(props: FocusableRowProps) {
		super(props);
		this.state = {
			isFocused: false
		};
	}

	private onMouseEnter = () => {
		this.setState({
			isFocused: true
		});
	};

	private onMouseLeave = () => {
		this.setState({
			isFocused: false
		});
	};

	public render() {
		const { children } = this.props;
		const childrenWithFocusProp = React.Children.map(children, (child: React.ReactElement<any>) => {
			return React.cloneElement(child, {
				isFocused: this.state.isFocused
			})
		});

		return (
			<StyledListRow
				className={this.props.className}
				flexDirection={'row'}
				alignItems={'stretch'}
				isFocused={this.state.isFocused}
				onMouseOver={this.onMouseEnter}
				onMouseLeave={this.onMouseLeave}
			>
				{childrenWithFocusProp}
			</StyledListRow>
		);
	}
}
