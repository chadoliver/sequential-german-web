import * as color from 'color';

export let teal = color('#0f8B8D');
export let orange = color('#EC9A29');
export let brown = color('#654236');
export let khaki = color('#D6D4A0');
export let tangerine = color('#E24E1B');

export let white = color('#ffffff');
export let red = color('#d94941');
export let borderGrey = color('#d9d9d9');

export let listItemBackground = color('#f8f8f8');
export let subtitle = color('#888');
export let mediaControls = color('#bbb');
