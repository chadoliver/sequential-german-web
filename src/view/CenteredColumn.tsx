import * as React from 'react';
import styled from 'styled-components';

const Container = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: flex-start;
	position: relative;
	flex-grow: 1;
`;

interface ColumnProps {
	maxWidth: string;
}

const Column = styled.div`
	width: 100%;
	max-width: ${(props: ColumnProps) => props.maxWidth};
	flex-grow: 1;
`;

export type CenteredColumnProps = {
	width: string;
	children: JSX.Element;
};

const CenteredColumn: React.SFC<CenteredColumnProps> = ({width, children}) => {
	return (
		<Container>
			<Column maxWidth={width}>
				{children}
			</Column>
		</Container>
	);
};

export default CenteredColumn;
